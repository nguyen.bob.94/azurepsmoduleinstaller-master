function Install-PSModule
{
  #Universal variable for set Documents folder in your profile. Better to use this if Documents folders are define with custom path or using OneDrive.
  $PWDocuments = [environment]::getfolderpath("mydocuments")

  #Get-Credential - Prompts for user input of Azure credentials
  #These credentials will be stored in the $NewCred variable
  $NewCred = Get-Credential -Message "Please enter your Azure admin credentials"

  #Get-Credential - Prompts for user input of Azure credentials
  #These credentials will be stored in the $NewCred variable
  #Export an encrypted credential file onto user's document folder, using the $NewCred variable
  Write-Output "Exporting credentials to an encrypted file and storing as $PWDocuments\Azurecredentials.dat"
  $NewCred | Export-Clixml $PWDocuments\Azurecredentials.dat

  #Check if Powershell Exist in user profile
  $CheckPSProfile = Test-Path -Path $PWDocuments\WindowsPowershell\Microsoft.PowerShell_Profile.ps1
  $CheckPSISEProfile = Test-Path -Path $PWDocuments\WindowsPowershell\Microsoft.PowerShellISE_Profile.ps1
  $ProfileDir = Test-Path -Path $PWDocuments\WindowsPowershell
  $String1 = '$PWDocuments = [environment]::getfolderpath("mydocuments")'
  $String2 = '. "$PWDocuments\PS-Functions\ConnectTo-Azure.ps1"'

  #Create WindowsPowershell folder if doesn't exist
  if($ProfileDir -eq "true")
  {
    Write-Output "WindowsPowershell folder found. Skipping creation"

  }
  else
  {
    mkdir $PWDocuments\WindowsPowershell

  }

  #If PS profile file exist, append $Content string to existing profile
  if($CheckPSProfile -eq "True")
  {
    Add-Content -Path "$PWDocuments\WindowsPowershell\Microsoft.PowerShell_Profile.ps1" "`n$String1"
    Add-Content -Path "$PWDocuments\WindowsPowershell\Microsoft.PowerShell_Profile.ps1" "$String2"

  }
  else
  {
    Write-Output $String1 > $PWDocuments\WindowsPowershell\Microsoft.PowerShell_Profile.ps1
    Add-Content -Path "$PWDocuments\WindowsPowershell\Microsoft.PowerShell_Profile.ps1" "$String2"

  }

  if($CheckPSISEProfile -eq "True")
  {
    Add-Content -Path "$PWDocuments\WindowsPowershell\Microsoft.PowerShellISE_Profile.ps1" "`n$Content1"
    Add-Content -Path "$PWDocuments\WindowsPowershell\Microsoft.PowerShellISE_Profile.ps1" "$Content2"

  }
  else
  {
    Write-Output $String1 > $PWDocuments\WindowsPowershell\Microsoft.PowerShellISE_Profile.ps1
    Add-Content -Path "$PWDocuments\WindowsPowershell\Microsoft.PowerShellISE_Profile.ps1" "$String2"

  }

  #Install functions in user profile Documents folder
  Write-Output "Creating Powershell function folder: $PWDocuments\PS-Functions"
  Copy-Item .\PS-Functions $PWDocuments -Force -Recurse

  #Getting Powershell to trust the scripts because Microsoft has trust and self conscious issues.
  Unblock-File -Path $PWDocuments\WindowsPowershell\Microsoft.PowerShell_profile.ps1
  Unblock-File -Path $PWDocuments\WindowsPowershell\Microsoft.PowerShellISE_Profile.ps1
  Unblock-File -Path $PWDocuments\PS-Functions\ConnectTo-Azure.ps1

  Write-Output ""
  Write-Output ""
  Write-Output "Installing Azure modules for Powershell completed.."
  Write-Output "To initialise connection to the Tenant at anytime, call the cmdlet: ConnectTo-Azure"
  Write-Output ""

}

#Script entry point
cd $PSScriptRoot

#Install the required modules

#Array of Modules
Write-Output "Installing the required Powershell Modules"
$ArrayModules = @("ExchangeOnlineManagement","AzureAD","MSOnline","Microsoft.Graph.Intune","Microsoft.Graph","Microsoft.Online.SharePoint.PowerShell","Az")

foreach($Module in $ArrayModules)
{
  $CheckModuleInstalled = Get-InstalledModule -Name $Module -ErrorAction Ignore

  if(!$CheckModuleInstalled)
  {
    Install-Module $Module -Scope CurrentUser -Confirm:$false -Allowclobber -Force -Verbose

  }
  elseif($CheckModuleInstalled)
  {
    Write-Output "Please uninstall modules before reinstalling again."
    exit

  }

}

#Install or append Powershell profiles
Install-PSModule
exit
